<?php

namespace App\Http\Controllers;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Log;
use Response;
use Storage;
use Validator;
use View;

class UploadController extends Controller
{


    public function show()
    {
        return view('akb2.image');
    }

    public function delete(Request $request)
    {
        
       
        Storage::disk('public')->delete($request->file);
        return response()->json(['url' => $request->file, 'action' => 'delete'], 200);
    }


    public function post_upload()
    {

        $input = Input::all();

   
        $rules = array(
            'file' => 'image|max:3000',
        );

        $validation = Validator::make($input, $rules);

        if ($validation->fails()) {
            return Response::make($validation->errors->first(), 400);
        }

        $file = Input::file('file');

        if (isset($file)) {

            $path = Storage::disk('public')->putFile(
                'temp',
                $file
            );
        }

        if ($path) {

            return response()->json(['url' => $path, 'action' => 'upload in server'], 200);
        } else {
            return Response::json('error', 400);
        }
    }
}
