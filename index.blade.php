<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/dropzone.min.css"
        integrity="sha512-3g+prZHHfmnvE1HBLwUnVuunaPOob7dpksI7/v6UnF/rnKGwHf/GdEq9K7iEN7qTtW+S0iivTcGpeTBqqB04wA=="
        crossorigin="anonymous" />
    <style>
        #dropzone {
            margin-bottom: 3rem;
        }

        .dropzone {
            border: 2px dashed #0087F7;
            border-radius: 5px;
            background: white;
        }

        .dropzone .dz-message {
            font-weight: 400;
        }

        .dropzone .dz-message .note {
            font-size: 0.8em;
            font-weight: 200;
            display: block;
            margin-top: 1.4rem;
        }

        *,
        *:before,
        *:after {
            box-sizing: border-box;
        }
    </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div id="dropzone">
        <form action="/upload" class="dropzone needsclick dz-clickable" id="myAwesomeDropzone">

            <div class="dz-message needsclick">
                <button type="button" class="dz-button">Drop files here or click to upload.</button><br>
                <span class="note needsclick">(This is just a demo dropzone. Selected files are <strong>not</strong>
                    actually uploaded.)</span>
            </div>     

        </form>
    </div>


    <div id="principal"></div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"
        integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg=="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.1/dropzone.min.js"
        integrity="sha512-Dq6kqg3S+qRPKNx2EHFC+qEHC2TkX8WNFQymh1FxCeurkVEeYknooSSjAFNeLhEZuOl6lfLULnl9kFLJWIn43w=="
        crossorigin="anonymous"></script>

    <script>
        fileList = new Array();

Dropzone.options.myAwesomeDropzone = {
  paramName: "file", // The name that will be used to transfer the file
  maxFilesize: 2, // MB
  //maxFiles:2,
  acceptedFiles: ".jpeg,.jpg,.png,.gif",
  addRemoveLinks:true,
  
  init: function() {
/* 
    var mockFile = { name: "myimage.jpg", size: 12345, type: 'image/jpeg' };       
   this.options.addedfile.call(this, mockFile);
   this.options.thumbnail.call(this, mockFile, "storage/temp/ahp4IRR1dxb7IZ2xGY9z91KcUBogtVqEuOTcYOQC.jpeg");
   mockFile.previewElement.classList.add('dz-success');
   mockFile.previewElement.classList.add('dz-complete'); */
         
        this.on("success", function(file, serverFileName) {
            console.log(serverFileName)
            fileList[file.name] = {"fid" : serverFileName['url'] };

            var input_name = "imagen[]";
            var input_value = serverFileName['url'] ;
            var hfield = '<input type="text" name="'+input_name+'"'+' value="'+input_value+'" />';
            $(hfield).appendTo($('#principal'));

        })
    },

    removedfile: function(file) {
  try {
            $.ajax({
                type: 'DELETE',
                url: "upload",
                data:  { file :  fileList[file.name].fid },
                success: function (response) {
                    console.log(response)
                    $('#principal :input[value="'+fileList[file.name].fid+'"]').remove();
                }
            });
            file.previewElement.remove();
       } catch (error) {
            file.previewElement.remove();
            
        }  
      }
 
  
};


 
    </script>
</body>

</html>