# Dropzone Laravel

## 1 Middleware Http
VerifyCsrfToken.php  protected $except = [ 'upload' ];

## 2 ADD Route 
Route::post('upload', 'UploadController@post_upload');

Route::get('upload', 'UploadController@show');

Route::delete('upload', 'UploadController@delete');


